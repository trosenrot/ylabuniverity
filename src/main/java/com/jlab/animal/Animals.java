package main.java.com.jlab.animal;

public abstract class Animals {
    public static Long maxValue;
    protected String name;
    protected boolean sleep = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected String allFields() {
        return String.format("name: %s", name);
    }

    public String sleep() {
        sleep = true;
        return name + " спит";
    }

    public String eat() {
        sleep = false;
        return name + " ест";
    }

    public String voice() {
        if (sleep) {
            return name + " спит!";
        }
        else {
            return getVoice();
        }
    }

    public abstract String getVoice();
}
