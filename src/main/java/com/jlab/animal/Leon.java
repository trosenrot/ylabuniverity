package main.java.com.jlab.animal;

public class Leon extends Animals {

    protected String allFields() {
        return String.format("name: %s", name);
    }

    @Override
    public String getVoice() {
        return "Ррррррррррр";
    }

    @Override
    public String toString() {
        return allFields();
    }

}
