package main.java.com.jlab.animal;

public class Cat extends Animals {
    private Animals other;

    public Animals getOther() {
        return other;
    }

    protected String allFields() {
        return String.format("name: %s", name);
    }

    @Override
    public String getVoice() {
        return "Мяу";
    }

    @Override
    public String toString() {
        return allFields();
    }

}
