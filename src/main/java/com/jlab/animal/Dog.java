package main.java.com.jlab.animal;

public class Dog extends Animals {

    protected String allFields() {
        return String.format("name: %s", name);
    }

    @Override
    public String getVoice() {
        return "Гав!";
    }

    @Override
    public String toString() {
        return allFields();
    }
}
