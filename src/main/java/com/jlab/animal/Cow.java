package main.java.com.jlab.animal;

public class Cow extends Animals {

    protected String allFields() {
        return String.format("name: %s", name);
    }

    @Override
    public String getVoice() {
        return "Мууууу";
    }

    @Override
    public String toString() {
        return allFields();
    }
}
