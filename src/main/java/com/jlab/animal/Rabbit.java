package main.java.com.jlab.animal;

public class Rabbit extends Animals {

    protected String allFields() {
        return String.format("name: %s", name);
    }

    @Override
    public String getVoice() {
        return "";
    }

    @Override
    public String toString() {
        return allFields();
    }
}
