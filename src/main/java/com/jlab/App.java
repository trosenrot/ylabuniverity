package main.java.com.jlab;

import main.java.com.jlab.animal.*;

import java.util.Calendar;

public class App {

    public static void main(String[] args) {
        Animals cat = new Cat();
        cat.setName("Мурка");
        System.out.println(cat.voice());
        System.out.println(cat.getName());
        System.out.println(cat.sleep());
        System.out.println(cat.eat());
        System.out.println(cat.sleep());
        System.out.println(cat.voice());
        Animals dog = new Dog();
        dog.setName("Жора");
        System.out.println(dog.getName());
        System.out.println(dog.sleep());
        System.out.println(dog.eat());
        System.out.println(dog.voice());
        System.out.println(dog.sleep());
        System.out.println(dog.voice());
    }
}
